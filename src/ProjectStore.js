import { Store } from 'laco';

export const ProjectStore = new Store({
    loginInputObj: {
        email: '',
        password: '',
    },
    isTermsConditions: false,
    isConditionPopUpOPen: false,
    productList: [],
    eachProductList: [],
    countObj: {
        product: 0,
    },
}, 'projectStore')