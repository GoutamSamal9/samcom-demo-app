import axios from "axios";

export const getProductList = async () => {
    const response = await axios.get(`http://localhost:3001/products`);
    const { data } = response;
    return data;
};

// delete http://localhost:3001/products/

export const removeProduct = async (id) => {
    const response = await axios.delete(`http://localhost:3001/products/${id}`);
    const { data } = response;
    return data;
};
