
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { makeStyles, Box, TextField, Button, FormControlLabel, Checkbox } from "@material-ui/core";
import { ProjectStore } from "../ProjectStore";
import { useStore } from 'laco-react';

const useStyles = makeStyles((theme) => ({
    inputContainer: {
        width: 600,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: theme.spacing(30),
    },
    textField: {
        marginTop: theme.spacing(3)
    },
    logInBtn: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: theme.spacing(5)
    }
}))

const LogInInput = () => {

    const classes = useStyles();

    let history = useHistory();

    const { loginInputObj, isTermsConditions } = useStore(ProjectStore);

    const [errorObj, setErrorObj] = useState({});

    const validateForm = () => {

        let errorList = {};

        if (!loginInputObj.email) {
            errorList.email = 'Please enter your  email !';
        }
        if (!loginInputObj.password) {
            errorList.password = 'Please enter your  password !';
        }

        if (loginInputObj.email !== 'samcom@gmail.com' && loginInputObj.email !== 'samcomTechnobrains@gmail.com') {
            errorList.email = 'Please proper email !';
        }

        if (loginInputObj.email === 'samcom@gmail.com' && loginInputObj.password !== '123') {
            errorList.password = 'Please valid password !';
        }
        if (loginInputObj.email === 'samcomTechnobrains@gmail.com' && loginInputObj.password !== 'sam123@') {
            errorList.password = 'Please valid password !';
        }

        if (Object.keys(errorList).length === 0) {
            setErrorObj({});
            return true;
        } else {
            setErrorObj(errorList);
            errorList = {};
            return false;
        }
    };

    const updateValue = (key, value) => {
        ProjectStore.set(
            () => ({ loginInputObj: { ...loginInputObj, [key]: value } }),
            'loginInputObj-onchange'
        );
    };

    const handelLogin = () => {
        if (!validateForm()) return;
        history.push("/home");
    }


    const handelTermsConditions = (value) => {
        ProjectStore.set(
            () => ({
                isTermsConditions: value,
                isConditionPopUpOPen: value ? true : false
            }),
            'project store conditions'
        );
    }

    return (
        <Box className={classes.inputContainer}>
            <Box>
                <TextField
                    required
                    error={!!errorObj.email}
                    helperText={errorObj.email}
                    id="standard-disabled"
                    defaultValue="email"
                    type="email"
                    placeholder="Eg-: goutam@gmail.com"
                    fullWidth
                    className={classes.textField}
                    onChange={(event) => updateValue('email', event.target.value)}
                    value={loginInputObj.email}
                />

                <TextField
                    required
                    error={!!errorObj.password}
                    helperText={errorObj.password}
                    id="standard-disabled"
                    defaultValue="password"
                    type="password"
                    fullWidth
                    placeholder="Eg-: goutam@12"
                    className={classes.textField}
                    onChange={(event) => updateValue('password', event.target.value)}
                    value={loginInputObj.password}
                />
                <Box>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={isTermsConditions}
                                onChange={(event) => handelTermsConditions(event.target.checked)}
                                name="Absent"
                                color="primary"
                            />
                        }
                        label="Terms and Conditions"
                    />
                </Box>
                <Box className={classes.logInBtn}>
                    <Button
                        color="primary"
                        variant="contained"
                        onClick={handelLogin}
                    >
                        LogIn
                    </Button>
                </Box>

            </Box>
        </Box >
    );
}
export default LogInInput;