import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Box from '@material-ui/core/Box';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { ProjectStore } from '../ProjectStore';
import { useStore } from 'laco-react';

const useStyles = makeStyles({

    media: {
        height: 440,
    },
});

export default function ProductCard({ each }) {

    const classes = useStyles();

    const { eachProductList } = useStore(ProjectStore);

    const handelAdd = (item) => {

        ProjectStore.set(
            () => ({
                eachProductList: [item, ...eachProductList],
                countObj: {
                    product: eachProductList.length + 1,
                }
            }),
            'loginInputObj-onchange'
        );
    }


    return (
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image={each.image}
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h3">
                        {each.name}
                    </Typography>
                    <Box display="flex" >
                        <Typography gutterBottom variant="h6" component="h2" style={{ fontWeight: 600 }}>
                            {each.price} ₹
                        </Typography>
                        <Typography gutterBottom variant="h6" component="h2" style={{ marginLeft: 10 }} >
                            {each.brand}
                        </Typography>
                    </Box>

                    <Typography variant="body2" color="textSecondary" component="p">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button size="small" color="primary" onClick={() => handelAdd(each)} >
                    Add To Cart
                </Button>

            </CardActions>
        </Card>
    );
}
