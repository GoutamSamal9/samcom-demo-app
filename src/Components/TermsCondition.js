import * as React from 'react';
import { Box, IconButton, DialogContent, DialogContentText, Dialog, DialogTitle, Divider } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';
import { useStore } from 'laco-react';

import { ProjectStore } from '../ProjectStore';


export default function TermsCondition() {

    const { isConditionPopUpOPen } = useStore(ProjectStore);

    const handleClose = () => {
        ProjectStore.set(
            () => ({
                isConditionPopUpOPen: false,

            }),
            'project store conditions'
        );
    };

    return (
        <Dialog
            open={isConditionPopUpOPen}
            onClose={handleClose}
        >
            <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title" >
                <Box display="flex" justifyContent="space-between" >
                    <Box>
                        Terms and Conditions
                    </Box>
                    <Box>
                        <IconButton onClick={handleClose} >
                            <ClearIcon />
                        </IconButton>
                    </Box>
                </Box>
                <Divider />
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    <Box pb={2}>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    </Box>
                </DialogContentText>
            </DialogContent>
        </Dialog>

    );
}
