import React from "react";
import { makeStyles, Grid, Box, Container } from "@material-ui/core";
import LoginImg from '../imgs/logIn.svg';
import LogInInput from "./LogInInput";

const useStyles = makeStyles((theme) => ({
    logInContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logInSubContainer: {
        height: '800vh',
        width: '100%',
        maxWidth: 1300,
        maxHeight: 700,
        marginTop: theme.spacing(7),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logInImage: {
        padding: 10,
        width: 600
    }
}))

const LogInScreen = () => {
    const classes = useStyles();
    return (
        <Box className={classes.logInContainer}>
            <Box className={classes.logInSubContainer}>
                <Container>
                    <Grid container spacing={3}>
                        <Grid item lg={6}>
                            <img alt="loginScreenImg" src={LoginImg} className={classes.logInImage} />
                        </Grid>
                        <Grid item lg={6}>
                            <LogInInput />
                        </Grid>
                    </Grid>
                </Container>
            </Box>

        </Box>
    );
}
export default LogInScreen;