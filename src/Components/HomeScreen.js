import React, { useEffect, useState } from "react";
import { Box, Grid, makeStyles, IconButton, Badge, InputBase } from "@material-ui/core";
import { Link } from "react-router-dom";
import SearchIcon from '@material-ui/icons/Search';
import ProductCard from "./ProductCard";
import { getProductList } from "../EndPoint";
import { useStore } from "laco-react";
import { ProjectStore } from "../ProjectStore";
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';

const useStyles = makeStyles((theme) => ({
    container: {
        maxWidth: 1200,
        margin: 'auto',
        paddingTop: theme.spacing(5)
    },
    iconButton: {
        position: 'fixed',
        bottom: 50,
        right: 50
    },
    inputRoot: {
        border: 'none',
        borderRadius: 5,
        background: '#e0eaf4',
        width: '100%',
        [theme.breakpoints.down('sm')]: {
            minWidth: 'auto'
        }

    },

    inputSearch: {
        fontSize: 25,
        margin: theme.spacing(0, 1.25),
        color: theme.palette.grey['A100']
    },

    inputInput: {
        padding: '13.5px 16px'
    },
    searchContainer: {
        margin: theme.spacing(1.2, 3),
    },
}))

const HomeScreen = () => {

    const classes = useStyles();

    const { productList, countObj } = useStore(ProjectStore);

    const [search, setSearch] = useState("");



    const getData = () => {
        getProductList().then((response) => {
            ProjectStore.set(
                () => ({
                    productList: [...response]
                }),
                'loginInputObj-onchange'
            );
        })
    }

    useEffect(() => {
        if (!productList.length) getData();
    }, [productList])


    const filteredProduct = productList.filter((product) => {
        return product.name.toLowerCase().includes(search.toLowerCase());
    });



    return (
        <>
            <Box className={classes.container}>
                <Box my={4}>
                    <InputBase
                        placeholder="Search Product...."
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                        inputProps={{ 'aria-label': 'search' }}
                        endAdornment={<SearchIcon className={classes.inputSearch} />}
                        margin="dense"
                        fullWidth
                        variant="outlined"
                        onChange={(e) => setSearch(e.target.value)}
                    />
                </Box>
                <Grid container spacing={3}>
                    {filteredProduct.map((each) => (
                        <Grid item lg={4} md={6} sm={12} >
                            <ProductCard key={each.id} each={each} />
                        </Grid>
                    ))
                    }
                </Grid>
            </Box >
            <Box className={classes.iconButton} title="Total product">
                <Link to='/my-product'>
                    <IconButton color="primary" >
                        <Badge badgeContent={countObj.product} color="secondary">
                            <AddShoppingCartIcon color="primary" />
                        </Badge>
                    </IconButton>
                </Link>
            </Box>

        </>
    );
}
export default HomeScreen;