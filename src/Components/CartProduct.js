import React from "react";
import { Box, Grid, makeStyles, IconButton, Badge, } from "@material-ui/core";
import AddProduct from "./AddProduct";
import { useStore } from "laco-react";
import { ProjectStore } from "../ProjectStore";
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';

const useStyles = makeStyles((theme) => ({
    container: {
        maxWidth: 1200,
        margin: 'auto',
        paddingTop: theme.spacing(5)
    },
    iconButton: {
        position: 'fixed',
        bottom: 50,
        right: 50
    },
}))

const CartProduct = () => {

    const classes = useStyles();

    const { eachProductList } = useStore(ProjectStore);

    let totalPrice = 0;

    for (let i = 0; i < eachProductList.length; i++) {
        totalPrice = totalPrice + eachProductList[i].price
        console.log(totalPrice);
    }


    return (
        <>
            <Box className={classes.container}>

                <Grid container spacing={3}>
                    {eachProductList.map((each) => (
                        <Grid item lg={4} md={6} sm={12} >
                            <AddProduct key={each.id} each={each} />
                        </Grid>
                    ))
                    }
                </Grid>
            </Box >
            <Box className={classes.iconButton} title="Total price">

                <IconButton color="primary"  >
                    <Badge
                        badgeContent={totalPrice}
                        max={totalPrice + totalPrice}
                        color="secondary">
                        <MonetizationOnIcon color="primary" />
                    </Badge>
                </IconButton>
            </Box>
        </>
    );
}
export default CartProduct;