import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { ProjectStore } from '../ProjectStore';
import { useStore } from 'laco-react';
import { removeProduct } from '../EndPoint';

const useStyles = makeStyles({

    media: {
        height: 440,
    },
});

export default function AddProduct({ each }) {

    const classes = useStyles();

    const { eachProductList } = useStore(ProjectStore);

    useEffect(() => {

    }, [eachProductList])

    const handelDelete = (itemId) => {
        console.log(itemId);
        removeProduct(itemId).then(() => {
            let index = eachProductList.findIndex(item => item.id === itemId);
            let list = [...eachProductList];
            list.splice(index, 1);
            ProjectStore.set(() => ({
                eachProductList: list, countObj: {
                    product: eachProductList.length - 1
                }
            }));

        })
    }




    return (
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image={each.image}
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {each.name}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button size="small" color="primary" onClick={() => handelDelete(each.id)} >
                    Remove
                </Button>

            </CardActions>
        </Card>
    );
}
