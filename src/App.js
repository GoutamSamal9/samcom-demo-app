import React from "react";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import { Box } from '@material-ui/core';
import LogInScreen from "./Components/LogInScreen";
import TermsCondition from "./Components/TermsCondition";
import HomeScreen from "./Components/HomeScreen";
import CartProduct from "./Components/CartProduct";

function App() {
  return (
    <Router>
      <Box>
        <Switch>
          <Route exact path="/">
            <LogInScreen />
          </Route>
          <Route exact path="/home">
            <HomeScreen />
          </Route>
          <Route exact path="/my-product">
            <CartProduct />
          </Route>
        </Switch>
      </Box>
      <TermsCondition />
    </Router>
  );
}

export default App;
